import java.util.UUID;
import java.util.concurrent.TimeUnit;

import junit.framework.TestCase;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class JamaTests extends TestCase {

	private WebDriver driver;
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		driver = new FirefoxDriver();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
	}
	
	// Tests adding a new feature
	
	// I have chosen JUnit here, because it is easy to throw a test together
	// with built-in asserts, setup, teardown, etc.  Normally, pages would be 
	// defined in classes, or some sort of map-file structure, to provide maximum
	// reusability.  This is the quick-and-dirty version of a test.  Since this
	// is a workflow test, there are multiple asserts used.
	
	// I have used mostly XPath selectors for the elements in this test, because
	// of the ability to find by text.  The CSS :contains pseudo doesn't work in
	// every browser, and is, apparently, no longer a part of the standard.
	// However, XPath has drawbacks, and CSS is the recommended standard  in Se,
	// so in a framework setting, I would have written methods to find elements 
	// using CSS selectors and inner text.
	
	@Test
	public void testAddNewFeature() {
		// Go to home screen and log in
		driver.navigate().to("http://testip.jamaland.com");
		driver.findElement(By.cssSelector("input#j_username")).sendKeys("jbrewer");
		driver.findElement(By.cssSelector("input#j_password")).sendKeys("12341234");
		driver.findElement(By.cssSelector("input#loginButton")).click();
		
		// Set a standard wait for 15 seconds
		WebDriverWait wait = new WebDriverWait(driver, 15);
		
		// Navigate through menus
		String menuItemXpath = "//div[contains (@class, 'x-tree-node-el')][normalize-space()='%s']";
		WebElement featureItem = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(String.format(menuItemXpath, "Features"))));
		assertNotNull(featureItem);
		Actions action = new Actions(driver);
		action.contextClick(featureItem).perform();
		String subMenuItemXpath = "//li[contains (@class, 'x-menu-list-item')][normalize-space()='%s']";
		WebElement addMenuItem = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(String.format(subMenuItemXpath, "Add"))));
		assertNotNull(addMenuItem);
		addMenuItem.click();
		WebElement newItemMenuItem = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(String.format(subMenuItemXpath, "New Item"))));
		assertNotNull(newItemMenuItem);
		newItemMenuItem.click();
		WebElement featureMenuItem = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(String.format(subMenuItemXpath, "Feature"))));
		assertNotNull(featureMenuItem);
		featureMenuItem.click();
		
		// Wait for the "Add Feature" window
		WebElement addFeatureWindow = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[contains(@class,'x-window')]//div[contains(@class,'j-item-header')][contains(.,'Feature')]")));
		assertNotNull(addFeatureWindow);
		
		// Click the "Save and Close" button
		WebElement saveAndCloseButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[normalize-space()='Save and Close']")));
		assertNotNull(saveAndCloseButton);
		saveAndCloseButton.click();
		
		// Verify the expected error when failing to enter the required fields
		WebElement errorPanel = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.j-error-panel")));
		assertNotNull(errorPanel);
		assertEquals("you are missing some required fields", errorPanel.getText().trim().toLowerCase());
		
		// Enter the name for the new feature, using a UUID for randomness so the test can be run multiple times without refreshing data
		WebElement nameTextBox = driver.findElement(By.xpath("//div[contains(@class,'x-form-item')][normalize-space()='*Name:']//input"));
		assertNotNull(nameTextBox);
		String uniqueString = UUID.randomUUID().toString().substring(0, 5);
		nameTextBox.sendKeys(uniqueString + " feature");
		
		// Select the priority for the new feature
		WebElement prioritySelector = driver.findElement(By.xpath("//div[contains(@class,'x-form-item')][normalize-space()='Priority:']//input"));
		assertNotNull(prioritySelector);
		prioritySelector.click();
		WebElement priorityMediumItem = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[contains(@class,'x-combo-list-item')][normalize-space()='Medium']")));
		assertNotNull(priorityMediumItem);
		priorityMediumItem.click();
		
		// Unselect the "Notify" checkbox if it is selected
		WebElement notifyCheckbox = driver.findElement(By.xpath("//div[contains(@class,'x-form-check-wrap')][contains(.,'Notify')]/input"));
		assertNotNull(notifyCheckbox);
		
		if (notifyCheckbox.isSelected())
		{
			notifyCheckbox.click();
		}
		
		assertEquals(false, notifyCheckbox.isSelected());
		
		// For some reason, Selenium thinks the "Save and Close" button is clickable, but something is causing it to act strangely
		// so, I have put in a *rare* hard delay, which would be thoroughly researched and removed if I could figure out the cause
		// of the problem
		try {
			Thread.sleep(2000);
		}
		catch (InterruptedException e)
		{
			//v don't much care
		}
		
		// Click the "Save and Close" button
		wait.until(ExpectedConditions.elementToBeClickable(saveAndCloseButton));
		saveAndCloseButton.click();
		
		// Wait for the "Add Feature" window to go away
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[contains(@class,'x-window')]//div[contains(@class,'j-item-header')][contains(.,'Features')]")));
		
		// Check the name in the new feature tab to ensure it matches the one it was given
		WebElement nameRow = wait.until(ExpectedConditions.presenceOfElementLocated((By.cssSelector("td[class*='j-item-field-name']"))));
		assertNotNull(nameRow);
		assertEquals(true, nameRow.getText().toLowerCase().contains(uniqueString.toLowerCase()));
		
		// Check the priority to ensure it matches the one selected
		WebElement priorityRow = driver.findElement(By.cssSelector("td[class*='j-item-field-priority']"));
		assertNotNull(priorityRow);
		assertEquals(true, priorityRow.getText().toLowerCase().contains("medium"));
	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
		driver.close();
		driver.quit();
	}
	
}
